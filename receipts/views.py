from django.shortcuts import render,redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CreateExpenseForm, CreateAccountForm

# Create your views here.

@login_required
def receipt_list(request):
    list = Receipt.objects.filter(purchaser=request.user)
# you can access user data using request.user
    context = {
        "list": list,
    }
    return render(request, "receipts/receipt_list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm

    return render(request, "receipts/create_receipt.html", {'form': form})

@login_required
def category_list(request):
    category_name = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": category_name,
    }
    return render(request, "receipts/category_list.html", context)

@login_required
def account_list(request):
    account_user = Account.objects.filter(owner=request.user)
    context = {
        "accounts": account_user,
    }
    return render(request, "receipts/account_list.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateExpenseForm(request.POST)
        if form.is_valid():
            new_expense = form.save(False)
            new_expense.owner = request.user
            new_expense.save()
            #used path from models related name not url
            return redirect('/receipts/categories/')
    else:
        form = CreateExpenseForm

    return render(request, "receipts/create_category.html", {'form': form})


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            new_account.save()
            #used path from models related name not url
            return redirect('/receipts/accounts/')
    else:
        form = CreateAccountForm

    return render(request, "receipts/create_account.html", {'form': form})
