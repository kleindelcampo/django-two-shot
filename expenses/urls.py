from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from accounts import views


def receipt_list_redirect(request):
    return redirect("home")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("accounts/", include("accounts.urls")),
    path("", receipt_list_redirect),
]
